package com.fer.projekt.kontrolapristupa.models

import com.google.firebase.firestore.DocumentReference

data class User(
    var email: String? = null,
    var name: String? = null,
    @field:JvmField
    var isAdmin: Boolean? = null,
    var tickets: ArrayList<DocumentReference>? = null
)