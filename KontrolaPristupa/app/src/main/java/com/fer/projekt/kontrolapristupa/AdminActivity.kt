package com.fer.projekt.kontrolapristupa

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth


class AdminActivity : AppCompatActivity() {


    var mAuth: FirebaseAuth? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin)
        mAuth = FirebaseAuth.getInstance()
        val btnLogOut = findViewById<Button>(R.id.btnLogout)
        btnLogOut?.setOnClickListener {
            mAuth!!.signOut()
            startActivity(Intent(this@AdminActivity, LoginActivity::class.java))
        }
        val btnScan = findViewById<Button>(R.id.btnScan)
        btnScan.setOnClickListener { startActivity(Intent(this, ScannerActivity::class.java)) }

        val btnGenerate = findViewById<Button>(R.id.btnGenerateQR)
        btnGenerate.setOnClickListener { startActivity(Intent(this, QRGeneratorActivity::class.java)) }

        val btnEntrances = findViewById<Button>(R.id.btnEntrances)
        btnEntrances.setOnClickListener { startActivity(Intent(this, EntrancesActivity::class.java)) }


//        val db = Firebase.firestore
//
//        val user = FirebaseAuth.getInstance().currentUser
//        if (user != null) {
//            // Name, email address, and profile photo Url
//            val name = user.displayName
//            val email = user.email
//
//            // Check if user's email is verified
//            val emailVerified = user.isEmailVerified
//
//            // The user's ID, unique to the Firebase project. Do NOT use this value to
//            // authenticate with your backend server, if you have one. Use
//            // FirebaseUser.getIdToken() instead.
//            val uid = user.uid
//            Log.d("DB", uid)
//        }
//
//        db.collection("users")
//            .get()
//            .addOnSuccessListener { result ->
//                for (document in result) {
//                    Log.d(TAG, "${document.id} => ${document.data}")
//                }
//            }
//            .addOnFailureListener { exception ->
//                Log.w(TAG, "Error getting documents.", exception)
//            }

    }

    override fun onStart() {
        super.onStart()
        val user = mAuth!!.currentUser
        if (user == null) {
            startActivity(Intent(this@AdminActivity, LoginActivity::class.java))
        }
    }


}