package com.fer.projekt.kontrolapristupa.models

import com.google.firebase.Timestamp

data class Ticket(
    var dateCreated: Timestamp? = null,
    var dateUsed: Timestamp? = null,
    @field:JvmField
    var isUsed: Boolean? = null,
    var QRCode: String? = null
)
