package com.fer.projekt.kontrolapristupa

import android.content.ContentValues.TAG
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.Glide
import com.fer.projekt.kontrolapristupa.models.Ticket
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase


class MainActivity : AppCompatActivity() {
    var mAuth: FirebaseAuth? = null
    val db = Firebase.firestore
    var myUrl = "empty"
    var uid = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val user = FirebaseAuth.getInstance().currentUser
        if (user != null) {
            uid = user.uid
        }

        if (myUrl == "empty") {
            val textView: TextView = findViewById<View>(R.id.textView) as TextView
            textView.visibility = View.VISIBLE
            val title: TextView = findViewById<View>(R.id.title) as TextView
            title.visibility = View.GONE
        }

        refreshData()

        val btn = findViewById(R.id.btnHistory) as Button
        btn.setOnClickListener {
            startActivity(Intent(this@MainActivity, TicketHistoryActivity::class.java))
        }

        val pullToRefresh = findViewById<SwipeRefreshLayout>(R.id.pullToRefresh)
        pullToRefresh.setOnRefreshListener {
            val intent = intent
            finish()
            startActivity(intent)
            pullToRefresh.isRefreshing = true
        }

        mAuth = FirebaseAuth.getInstance()
        val btnLogOut = findViewById<Button>(R.id.btnLogout)
        btnLogOut?.setOnClickListener {
            mAuth!!.signOut()
            startActivity(Intent(this@MainActivity, LoginActivity::class.java))
        }
    }

    private fun refreshData() {
        val user = FirebaseAuth.getInstance().currentUser
        if (user != null) {
            uid = user.uid
        }


            val docRef = db.collection("users").document(uid)
                docRef.get()
                    .addOnSuccessListener { document ->
                        if (document != null) {
                            var tickets = ArrayList<DocumentReference>()
                            val data = document.data?.get("tickets")
                            if (data != null) {
                                tickets = data as ArrayList<DocumentReference>
                            }
                            var ticketModel = Ticket()
                            for (ticket in tickets) {
                                Log.d("ticket: ", ticket.toString())
                                ticket.get()
                                    .addOnSuccessListener { document ->
                                        if (document != null) {
                                            ticketModel = document.toObject<Ticket>()!!
                                            Log.d("ticketModel: ", ticketModel.toString())

                                            if (ticketModel.isUsed == false) {
                                                val textView: TextView =
                                                    findViewById<View>(R.id.textView) as TextView
                                                textView.visibility = View.GONE
                                                val title: TextView =
                                                    findViewById<View>(R.id.title) as TextView
                                                title.visibility = View.VISIBLE
                                                Log.d("isUsedif: ", ticketModel.isUsed.toString())
                                                myUrl = ticketModel.QRCode.toString()
                                                val imageView: ImageView =
                                                    findViewById<View>(R.id.imageView) as ImageView

                                                Glide.with(applicationContext)
                                                    .load(myUrl)
                                                    .into(imageView)

                                            } else {
                                                if (myUrl == "empty") {
                                                    val textView: TextView =
                                                        findViewById<View>(R.id.textView) as TextView
                                                    textView.visibility = View.VISIBLE
                                                    val title: TextView =
                                                        findViewById<View>(R.id.title) as TextView
                                                    title.visibility = View.GONE
                                                }
                                            }
                                        } else {
                                            Log.d(TAG, "No such document")
                                        }
                                    }
                                    .addOnFailureListener { exception ->
                                        Log.d(TAG, "get failed with ", exception)
                                    }
                            }
                        }
                    }
                    .addOnFailureListener { exception ->
                        Log.d(TAG, "get failed with ", exception)
                    }


    }
}