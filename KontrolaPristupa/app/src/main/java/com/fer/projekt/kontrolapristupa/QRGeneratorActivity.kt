package com.fer.projekt.kontrolapristupa

import android.content.ContentValues.TAG
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Point
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.Display
import android.view.WindowManager
import android.widget.*
import androidmads.library.qrgenearator.QRGContents
import androidmads.library.qrgenearator.QRGEncoder
import androidx.appcompat.app.AppCompatActivity
import com.fer.projekt.kontrolapristupa.models.Ticket
import com.fer.projekt.kontrolapristupa.models.User
import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import java.io.ByteArrayOutputStream
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*


class QRGeneratorActivity : AppCompatActivity() {
    val uids = mutableMapOf<String, String>()

    lateinit var qrIV: ImageView
    lateinit var msgEdt: EditText
    lateinit var generateQRBtn: Button

    lateinit var bitmap: Bitmap

    lateinit var qrEncoder: QRGEncoder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qrgenerator)

        getUsers()

        val dropdown = findViewById<AutoCompleteTextView>(R.id.idEdt)
        dropdown.setOnClickListener{dropdown.showDropDown()}

        qrIV = findViewById<ImageView>(R.id.idIVQrcode)
        msgEdt = findViewById(R.id.idEdt)
        generateQRBtn = findViewById(R.id.idBtnGenerateQR)

        generateQRBtn.setOnClickListener {
            if (TextUtils.isEmpty(msgEdt.text.toString())) {

                Toast.makeText(applicationContext, "Enter your message", Toast.LENGTH_SHORT).show()
            } else {
                val windowManager: WindowManager = getSystemService(WINDOW_SERVICE) as WindowManager

                val display: Display = windowManager.defaultDisplay

                val point: Point = Point()
                display.getSize(point)

                val width = point.x
                val height = point.y

                var dimen = if (width < height) width else height
                dimen = dimen * 3 / 4

                val formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss")
                val id = LocalDateTime.now().format(formatter)
                qrEncoder = QRGEncoder(id, null, QRGContents.Type.TEXT, dimen)

                try {

                    bitmap = qrEncoder.bitmap

                    qrIV.setImageBitmap(bitmap)

                    val baos = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                    val data = baos.toByteArray()

                    val storage = FirebaseStorage.getInstance()
                    val storageRef = storage.reference
                    val ticketRef = storageRef.child("qr$id.jpg")

                    val uploadTask: UploadTask = ticketRef.putBytes(data)
                    val urlTask = uploadTask.continueWithTask { task ->
                        if (!task.isSuccessful) {
                            task.exception?.let {
                                throw it
                            }
                        }
                        ticketRef.downloadUrl
                    }.addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            val downloadUri = task.result
                            Log.d("IMAGE URI: ", downloadUri.toString())
                            val ticket = Ticket(dateCreated = Timestamp.now(), dateUsed = null, isUsed = false, QRCode = downloadUri.toString())

                            val db = Firebase.firestore

                            val docId = uids[msgEdt.text.toString()]

                            val user = docId?.let { it1 -> db.collection("users").document(it1) }

                            db.collection("tickets").document(id)
                                .set(ticket)
                                .addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully written!") }
                                .addOnFailureListener { e -> Log.w(TAG, "Error writing document", e) }
                            val ref = db.document("tickets/$id")
                            user?.update("tickets", FieldValue.arrayUnion(ref))
                                ?.addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully updated!") }
                                ?.addOnFailureListener { e -> Log.w(TAG, "Error updating document", e) }


                        } else {
                            // Handle failures
                            // ...
                        }
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    fun getUsers() {
        val db = Firebase.firestore

        db.collection("users")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    val data = document.toObject(User::class.java)!!
                    Log.d(TAG, "${document.id} => ${data.email}")
                    data.email?.let { uids.put(it, document.id) }
                }
                val options = uids.keys.toTypedArray()
                Log.d("options: ", options.toString())
                val dropdown = findViewById<AutoCompleteTextView>(R.id.idEdt)
//                val emails: Array<out String> = ArrayList(options).toArray()
                ArrayAdapter(this, android.R.layout.simple_list_item_1, options).also { adapter ->
                    dropdown.setAdapter(adapter)
                }
//                dropdown.showDropDown()
            }
            .addOnFailureListener { exception ->
                Log.d(TAG, "Error getting documents: ", exception)
            }
            }
        }
