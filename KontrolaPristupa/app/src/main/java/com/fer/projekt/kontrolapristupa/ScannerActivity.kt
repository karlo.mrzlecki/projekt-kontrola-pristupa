package com.fer.projekt.kontrolapristupa

import android.content.ContentValues.TAG
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.budiyev.android.codescanner.*
import com.google.firebase.Timestamp
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase


class ScannerActivity : AppCompatActivity() {
    private lateinit var codeScanner: CodeScanner
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scanner)

        val scannerView = findViewById<CodeScannerView>(R.id.scanner_view)
        codeScanner = CodeScanner(this, scannerView)

        // Parameters (default values)
        codeScanner.camera = CodeScanner.CAMERA_BACK // or CAMERA_FRONT or specific camera id
        codeScanner.formats = CodeScanner.ALL_FORMATS // list of type BarcodeFormat,
        // ex. listOf(BarcodeFormat.QR_CODE)
        codeScanner.autoFocusMode = AutoFocusMode.SAFE // or CONTINUOUS
        codeScanner.scanMode = ScanMode.SINGLE // or CONTINUOUS or PREVIEW
        codeScanner.isAutoFocusEnabled = true // Whether to enable auto focus or not
        codeScanner.isFlashEnabled = false // Whether to enable flash or not

        // Callbacks
        codeScanner.decodeCallback = DecodeCallback {
            runOnUiThread {
//                val sharedPreferences: SharedPreferences =
//                    getSharedPreferences("prefs", MODE_PRIVATE)
//                val uid = sharedPreferences.getString("uid", "0")
                val db = Firebase.firestore
                try {
                    val ticketRef = db.collection("tickets").document(it.text)

                    ticketRef.get()
                        .addOnSuccessListener { document ->
                            if (document != null) {
                                Log.d(TAG, "DocumentSnapshot data: ${document.data}")
                                db.collection("tickets").document(it.text).get()
                                    .addOnCompleteListener { task ->
                                        if (task.isSuccessful) {
                                            // Document found in the offline cache
                                            val isUsed = task.result?.data?.get("isUsed")
                                            if (isUsed as Boolean) {
                                                //                                        Toast.makeText(this, "Karta je već iskorištena",
                                                //                                            Toast.LENGTH_LONG).show()

                                                val customToastLayout = layoutInflater.inflate(
                                                    R.layout.custom_toast,
                                                    null
                                                )
                                                val textView: TextView =
                                                    customToastLayout.findViewById(R.id.tvToast)
                                                textView.setBackgroundColor(Color.parseColor("#E6E61515"))
                                                textView.text = "Karta je već iskorištena!"
                                                val customToast = Toast(this)
                                                customToast.view = customToastLayout
                                                customToast.setGravity(Gravity.CENTER, 0, 0)
                                                customToast.duration = Toast.LENGTH_LONG
                                                customToast.show()

                                            } else {
                                                val customToastLayout = layoutInflater.inflate(
                                                    R.layout.custom_toast,
                                                    null
                                                )
                                                val textView: TextView =
                                                    customToastLayout.findViewById(R.id.tvToast)
                                                textView.setBackgroundColor(Color.parseColor("#E64CAF50"))
                                                textView.text = "Karta je valjana!"
                                                val customToast = Toast(this)
                                                customToast.view = customToastLayout
                                                customToast.setGravity(Gravity.CENTER, 0, 0)
                                                customToast.duration = Toast.LENGTH_LONG
                                                customToast.show()
                                                db.collection("tickets").document(it.text).update(
                                                    "isUsed",
                                                    true,
                                                    "dateUsed",
                                                    Timestamp.now()
                                                )
                                            }
                                        } else {
                                            Log.d(TAG, "Cached get failed: ", task.exception)
                                        }
                                    }
                                //                            Log.d("DATA: ", isUsed.toString())
                                //                            if (isUsed)
                            } else {
                                Log.d(TAG, "No such document")
                                val customToastLayout =
                                    layoutInflater.inflate(R.layout.custom_toast, null)
                                val textView: TextView =
                                    customToastLayout.findViewById(R.id.tvToast)
                                textView.setBackgroundColor(Color.parseColor("#E6E61515"))
                                textView.text = "Karta ne postoji u sustavu!"
                                val customToast = Toast(this)
                                customToast.view = customToastLayout
                                customToast.setGravity(Gravity.CENTER, 0, 0)
                                customToast.duration = Toast.LENGTH_LONG
                                customToast.show()
                            }
                        }
                        .addOnFailureListener { exception ->
                            Log.d(TAG, "get failed with ", exception)
                        }

                } catch (e: Exception){
                    val customToastLayout =
                        layoutInflater.inflate(R.layout.custom_toast, null)
                    val textView: TextView =
                        customToastLayout.findViewById(R.id.tvToast)
                    textView.setBackgroundColor(Color.parseColor("#E6E61515"))
                    textView.text = "Karta ne postoji u sustavu!"
                    val customToast = Toast(this)
                    customToast.view = customToastLayout
                    customToast.setGravity(Gravity.CENTER, 0, 0)
                    customToast.duration = Toast.LENGTH_LONG
                    customToast.show()
                } catch (e: Exception){
                    val customToastLayout =
                        layoutInflater.inflate(R.layout.custom_toast, null)
                    val textView: TextView =
                        customToastLayout.findViewById(R.id.tvToast)
                    textView.setBackgroundColor(Color.parseColor("#E6E61515"))
                    textView.text = "Karta ne postoji u sustavu!"
                    val customToast = Toast(this)
                    customToast.view = customToastLayout
                    customToast.setGravity(Gravity.CENTER, 0, 0)
                    customToast.duration = Toast.LENGTH_LONG
                    customToast.show()
                }
            }
        }
        codeScanner.errorCallback = ErrorCallback { // or ErrorCallback.SUPPRESS
            runOnUiThread {
                Toast.makeText(this, "Camera initialization error: ${it.message}",
                    Toast.LENGTH_LONG).show()
            }
        }

        scannerView.setOnClickListener {
            codeScanner.startPreview()
        }
    }

    override fun onResume() {
        super.onResume()
        codeScanner.startPreview()
    }

    override fun onPause() {
        codeScanner.releaseResources()
        super.onPause()
    }
}