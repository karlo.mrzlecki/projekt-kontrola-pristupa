package com.fer.projekt.kontrolapristupa

import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.fer.projekt.kontrolapristupa.databinding.ActivityEntrancesBinding
import com.fer.projekt.kontrolapristupa.models.Ticket
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import com.google.firebase.Timestamp
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*


class EntrancesActivity : AppCompatActivity() {
    val linevalues = ArrayList<Entry>()
    var ticketsByDate = sortedMapOf<String, Int>()
//    var ticketsByDateSorted = mutableMapOf<String, Int>()
    private lateinit var binding: ActivityEntrancesBinding
    var layout: LinearLayout? = null
    var tickets = HashMap<String, Ticket>()

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding = ActivityEntrancesBinding.inflate(layoutInflater)
        setContentView(binding.root)

        layout = findViewById<View>(R.id.cardcontainer) as LinearLayout

        prepareData(onSuccess = { showData() })


    }


    private fun addCard(ticketDate: Timestamp, ticketId: String, url: String) {
        val view: View = layoutInflater.inflate(R.layout.ticket_history, null)
        val date: TextView = view.findViewById(R.id.tvDate)
        date.text = ticketDate.toDate().toString()
        val id: TextView = view.findViewById(R.id.tvId)
        id.text = ticketId
        val image: ImageView = view.findViewById(R.id.ivQR)
        Glide.with(applicationContext)
            .load(url)
            .into(image)

        layout?.addView(view)
    }

    private fun prepareData(onSuccess: (() -> Unit)? = null, onError: ((errorMessage: String) -> Unit)? = null){
        val db = Firebase.firestore

        db.collection("tickets")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {

                    Log.d("DocumentData: ", document.data.toString())
//                    tickets = document.data as HashMap<String, Ticket>
                    tickets[document.id] = document.toObject<Ticket>()


                }
                onSuccess?.invoke()
            }
            .addOnFailureListener { exception ->
                if (onError != null) {
                    onError.invoke(exception.toString())
                }
            }
    }

    private fun showData(){
        var ticketModel = Ticket()
        val sdf = SimpleDateFormat("dd.MM")
        for (ticket in tickets) {
            Log.d("ticketyyy: ", ticket.toString())


            ticketModel = ticket.value
            val dateUsed = ticketModel.dateUsed
            val qr = ticketModel.QRCode
            val tid = ticket.key
            if (dateUsed != null) {
                val ticketDateString = sdf.format(dateUsed.toDate())
                if (qr != null) {
                    if (ticketsByDate.containsKey(ticketDateString)) {
                        ticketsByDate.merge(ticketDateString, 1, Int::plus)
                    } else {
                        ticketsByDate[ticketDateString] = 1
                    }
                    addCard(dateUsed, tid, qr)
                }
            }
        }

        val ticketsByDateSorted = ticketsByDate

//                    val ticketDate = ticket.value.dateUsed
//                    val id = ticket.key
//                    val url = ticketModel.QRCode
//                    if (ticketDate != null) {
//                        val ticketDateString = sdf.format(ticketDate.toDate())
//                        if (url != null) {
//                            if (ticketsByDate.containsKey(ticketDateString)) {
//                                ticketsByDate.merge(ticketDateString, 1, Int::plus)
//                            } else {
//                                ticketsByDate.put(ticketDateString, 1)
//                            }
//                            addCard(ticketDate, id.toString(), url)
//                        }
//                    }
//                }


        Log.d("poslije: ", "da")
        val count = ticketsByDate.values.sum()
        val countView: TextView = findViewById(R.id.tvNoEntrances)
        countView.text = "Ukupan broj posjeta: $count"

        val cal: Calendar = Calendar.getInstance()
        var day: String
        var month: String
        var dateFloat: Float
        var dates = mutableListOf<String>()
        Log.d("ticketsByDate: ", ticketsByDate.toString())
        for (ticket in ticketsByDate) {
//            cal.time = ticket.key.toDate()
//            day = cal.get(Calendar.DAY_OF_MONTH).toString()
//            month = cal.get(Calendar.MONTH).toString()
            dates.add(ticket.key+".")
            Log.d("ticket key float ", ticket.key.toFloat().toString())
            Log.d("ticket key float ", ticket.value.toFloat().toString())
//            dateFloat = "$day.$month".toFloat()
            linevalues.add(Entry(ticket.key.toFloat(), ticket.value.toFloat()))
        }
        val linedataset = LineDataSet(linevalues, "First")
        //We add features to our chart
        linedataset.color = resources.getColor(R.color.purple_200)

        linedataset.circleRadius = 10f
        linedataset.setDrawFilled(true)
        linedataset.valueTextSize = 20F
        linedataset.fillColor = resources.getColor(R.color.purple_200)
        linedataset.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        val data = LineData(linedataset)
        binding.getTheGraph.data = data
        val xAxis: XAxis = binding.getTheGraph.xAxis
        val yAxisLeft: YAxis = binding.getTheGraph.axisLeft
        val yAxisRight: YAxis = binding.getTheGraph.axisRight
        binding.getTheGraph.legend.isEnabled = false
        Log.d("dates", dates.toString())

        val formatter: ValueFormatter = object : ValueFormatter() {
            override fun getAxisLabel(value: Float, axis: AxisBase): String {
                return dates.getOrNull(value.toInt()) ?: value.toString()
            }
        }
        xAxis.granularity = 1f // minimum axis-step (interval) is 1
        yAxisLeft.granularity = 1f
        yAxisRight.isEnabled = false
//        xAxis.valueFormatter = formatter
        binding.getTheGraph.setBackgroundColor(resources.getColor(R.color.white))
        binding.getTheGraph.animateXY(2000, 2000, Easing.EaseInCubic)
    }
}
