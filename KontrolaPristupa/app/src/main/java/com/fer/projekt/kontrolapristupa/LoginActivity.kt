package com.fer.projekt.kontrolapristupa

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.fer.projekt.kontrolapristupa.models.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class LoginActivity : AppCompatActivity() {
    private var etLoginEmail: EditText? = null
    private var etLoginPassword: EditText? = null
//    private var tvRegisterHere: TextView? = null
//    private var btnLogin: Button? = null
    private var mAuth: FirebaseAuth? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val sharedPreferences: SharedPreferences =
            getSharedPreferences("prefs", MODE_PRIVATE)
        val isLoggedIn = sharedPreferences.getBoolean("isLoggedIn", false)

        etLoginEmail = findViewById<EditText>(R.id.etLoginEmail)
        etLoginPassword = findViewById<EditText>(R.id.etLoginPass)
//        tvRegisterHere = findViewById(R.id.tvRegisterHere)
//        btnLogin = findViewById(R.id.btnLogin)
        mAuth = FirebaseAuth.getInstance()
        val btnLogin = findViewById<Button>(R.id.btnLogin)
        btnLogin?.setOnClickListener { view -> loginUser() }
        val tvRegisterHere = findViewById<TextView>(R.id.tvRegisterHere)
        tvRegisterHere?.setOnClickListener(View.OnClickListener {
            startActivity(
                Intent(
                    this@LoginActivity,
                    RegisterActivity::class.java
                )
            )
        })
    }

    private fun loginUser() {
        val email: String = etLoginEmail?.getText().toString()
        val password: String = etLoginPassword?.getText().toString()
        if (TextUtils.isEmpty(email)) {
            etLoginEmail?.setError("Polje email ne može biti prazno")
            etLoginEmail?.requestFocus()
        } else if (TextUtils.isEmpty(password)) {
            etLoginPassword?.setError("Polje lozinka ne može biti prazno")
            etLoginPassword?.requestFocus()
        } else {
            mAuth!!.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Toast.makeText(
                            this@LoginActivity,
                            "Prijava uspješna",
                            Toast.LENGTH_SHORT
                        ).show()
                        val db = Firebase.firestore
                        var uid = ""

                        val user = FirebaseAuth.getInstance().currentUser
                        if (user != null) {
                            uid = user.uid
                        }

                        val sharedPreferences: SharedPreferences =
                            getSharedPreferences("prefs", MODE_PRIVATE)

                        val prefs: SharedPreferences.Editor = sharedPreferences.edit()

                        prefs.putString("uid", uid)

                        prefs.apply()

                        val isAdmin = false
                        val docRef = db.collection("users").document(uid)
                        var userC: User
                        docRef.get().addOnSuccessListener { documentSnapshot ->
                            userC = documentSnapshot.toObject(User::class.java)!!
                            if(userC.isAdmin == true){
                                val sharedPreferences: SharedPreferences =
                                    getSharedPreferences("prefs", MODE_PRIVATE)

                                val prefs: SharedPreferences.Editor = sharedPreferences.edit()

                                prefs.putBoolean("isLoggedIn", true)

                                prefs.apply()
                                startActivity(Intent(this@LoginActivity, AdminActivity::class.java))
                            }
                            else {
                                val sharedPreferences: SharedPreferences =
                                    getSharedPreferences("prefs", MODE_PRIVATE)

                                val prefs: SharedPreferences.Editor = sharedPreferences.edit()

                                prefs.putBoolean("isLoggedIn", true)

                                prefs.apply()
                                startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                            }
                        }
                    } else {
                        Toast.makeText(
                            this@LoginActivity,
                            "Greška prilikom prijave: " + task.exception!!.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
        }
    }
}