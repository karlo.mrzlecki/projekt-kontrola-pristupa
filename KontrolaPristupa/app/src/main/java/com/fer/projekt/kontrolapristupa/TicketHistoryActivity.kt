package com.fer.projekt.kontrolapristupa

import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.fer.projekt.kontrolapristupa.models.Ticket
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase


class TicketHistoryActivity : AppCompatActivity() {
    var layout: LinearLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ticket_history)

        layout = findViewById<View>(R.id.cardcontainer) as LinearLayout

        var uid = ""
        val db = Firebase.firestore
        val user = FirebaseAuth.getInstance().currentUser
        if (user != null) {
            uid = user.uid
        }

        var myUrl = "empty"
        val docRef = db.collection("users").document(uid)
        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    var tickets = ArrayList<DocumentReference>()
                    val data = document.data?.get("tickets")
                    if (data != null) {
                        tickets = data as ArrayList<DocumentReference>
                    }
                    var ticketModel = Ticket()
                    for (ticket in tickets){
                        Log.d("ticket: ", ticket.toString())
                        ticket.get()
                            .addOnSuccessListener { document ->
                                if (document != null) {
                                    ticketModel = document.toObject<Ticket>()!!
                                    Log.d("ticketModel: ", ticketModel.toString())
                                    if(ticketModel.isUsed == true){
                                        val ticketDate = ticketModel.dateUsed
                                        val id = document.id
                                        val url = ticketModel.QRCode
                                        if (ticketDate != null) {
                                            if (url != null) {
                                                addCard(ticketDate, id, url)
                                            }
                                        }
                                    }
                                } else {
                                    Log.d(ContentValues.TAG, "No such document")
                                }
                            }
                            .addOnFailureListener { exception ->
                                Log.d(ContentValues.TAG, "get failed with ", exception)
                            }
                    }
                }
            }
            .addOnFailureListener { exception ->
                Log.d(ContentValues.TAG, "get failed with ", exception)
            }
    }


    private fun addCard(ticketDate: Timestamp, ticketId: String, url: String) {
        val view: View = layoutInflater.inflate(R.layout.ticket_history, null)
        val date: TextView = view.findViewById(R.id.tvDate)
        date.text = ticketDate.toDate().toString()
        val id: TextView = view.findViewById(R.id.tvId)
        id.text = ticketId
        val image: ImageView = view.findViewById(R.id.ivQR)
        Glide.with(applicationContext)
            .load(url)
            .into(image)

        layout?.addView(view)
    }
}